// do some form stuff
let roboCheck = document.getElementById("roboCheck");
let submitButton = document.getElementById("submitButton");
let eventSelect = document.getElementById("eventSelect");
let eventTypeInput = document.getElementById("eventTypeInput");
let wallieCheck = document.getElementById("wallieCheck");
let sabrinaCheck = document.getElementById("sabrinaCheck");
let bothCheck = document.getElementById("bothCheck");

// show input if 'other' type of event is selected
eventSelect.addEventListener("change", function (event) {
    if (event.target.value === "other") {
        // show event type input and label
        eventTypeInput.classList.remove("hidden");
    } else {
        // hide event type input and label
        eventTypeInput.classList.add("hidden");
    }
});

// handle pet checkboxes
wallieCheck.addEventListener("change", function (event) {
    handleChecks(event.target, sabrinaCheck);
});

sabrinaCheck.addEventListener("change", function (event) {
    handleChecks(event.target, wallieCheck);
});

bothCheck.addEventListener("change", function (event) {
    if (!event.target.checked) {
        wallieCheck.checked = false;
        sabrinaCheck.checked = false;
    } else {
        wallieCheck.checked = true;
        sabrinaCheck.checked = true;
    }
});

// function to handle a pet checkbox
function handleChecks(thisCheck, otherCheck) {
    if (thisCheck.checked && otherCheck.checked) {
        bothCheck.checked = true;
    } else {
        bothCheck.checked = false;
    }
}

// make button disabled unless box is checked
roboCheck.addEventListener("click", function () {
    if (roboCheck.checked) {
        submitButton.removeAttribute("disabled")
    } else {
        submitButton.setAttribute("disabled", "");

    }
});
