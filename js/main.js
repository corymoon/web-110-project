// constants
const navButton = document.getElementById("navToggle");
const navMenu = document.getElementById("navMenu");

const closeIcon =
    '<svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" /></svg>';
const menuIcon =
    '<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h8m-8 6h16" /></svg>';

window.addEventListener("load", function () {
    navButton.innerHTML = menuIcon;
});

navButton.addEventListener("click", toggleNav);

function toggleNav() {
    let navOpen = navMenu.classList.contains("menu-open");
    if (navOpen) {
        navMenu.classList.remove("menu-open");
        navButton.innerHTML = menuIcon;
    } else {
        navMenu.classList.add("menu-open");
        navButton.innerHTML = closeIcon;
    }
}
